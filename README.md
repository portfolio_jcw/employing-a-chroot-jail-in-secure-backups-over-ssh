As a forerunner of current container techniques for isolating filesystems in Linux environments, chroot is underutilized in spite of being relatively simple to implement.  In this project, I used rsync to backup data to a personal cloud server via ssh tunnel, into a chroot jail.  While not a bulletproof security solution, this reduces the probability of exploit if the ssh login is compromised.

